/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.constants;

import org.springframework.beans.factory.annotation.Value;

/**
 * The Class LadConstants.
 *
 * @author kaushal.singh1
 */
public final class LadConstants {

  /** The Routing Constant for exposed APIs. */
  public static final String GET_LEARNING_EXPERIENCE = "REST/GET_LEARNINGEXPERIENCE";

  /** URL constants configured via properties. **/
  @Value("${lec.base.url}")
  public String lecBaseUrl;

  /** The Constant LEC_PRE_COMPOSE_EVENT. */
  public static final String LEC_LEARNING_EXPERIENCE_PRE_COMPOSED = "LEC_LEARNING_EXPERIENCE_PRE_COMPOSED";

  /** The Constant LAD_LEARNING_EXPERIENCE_READY. */
  public static final String LAD_LEARNING_EXPERIENCE_READY = "LAD_LEARNING_EXPERIENCE_READY";

  /** The Constant ISC_HANDLERS. */
  public static final String ISC_HANDLERS = "ISC_Handlers:";

  /**
   * Instantiates a new lad constants.
   */
  private LadConstants() {
    super();
  }

}
