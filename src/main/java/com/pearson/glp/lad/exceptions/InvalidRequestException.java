/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.exceptions;

/**
 * The Class InvalidRequestException.
 */
public class InvalidRequestException extends Exception {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 3786480010602972834L;

  /**
   * Instantiates a new invalid request exception.
   *
   * @param message
   *          the message
   */
  public InvalidRequestException(String message) {
    super(message);
  }

  /**
   * Instantiates a new invalid request exception.
   *
   * @param message
   *          the message
   * @param t
   *          the t
   */
  public InvalidRequestException(String message, Throwable t) {
    super(message, t);
  }

}