/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.service;

import com.pearson.glp.core.handlers.base.ServiceHandlerContext;
import com.pearson.glp.core.handlers.base.ServiceHandlerResponse;
import com.pearson.glp.core.handlers.isc.IscServiceCompensationHandlerContext;
import com.pearson.glp.core.handlers.isc.IscServiceHandlerContext;
import com.pearson.glp.core.handlers.isc.IscServiceHandlerVoidResponse;
import com.pearson.glp.core.handlers.isc.IscServiceRetryHandlerContext;

import reactor.core.publisher.Mono;

/**
 * The Interface AssetDeliveryService.
 *
 * @author dayanidhi.sadangi
 */
public interface AssetDeliveryService {

  /**
   * Fetch learning experiences.
   *
   * @param context
   *          the context
   * @return the mono
   */
  Mono<ServiceHandlerResponse> fetchLearningExperiences(ServiceHandlerContext context);

  /**
   * Lec pre compose event service handler.
   *
   * @param context
   *          the context
   * @return the mono
   */
  Mono<IscServiceHandlerVoidResponse> lecPreComposeEventServiceHandler(
      IscServiceHandlerContext context);

  /**
   * Lec pre compose event service compensation handler.
   *
   * @param context
   *          the context
   * @return the mono
   */
  Mono<IscServiceHandlerVoidResponse> lecPreComposeEventServiceCompensationHandler(
      IscServiceCompensationHandlerContext context);

  /**
   * Lec pre compose event service retry handler.
   *
   * @param context
   *          the context
   * @return the mono
   */
  Mono<IscServiceHandlerVoidResponse> lecPreComposeEventServiceRetryHandler(
      IscServiceRetryHandlerContext context);

}