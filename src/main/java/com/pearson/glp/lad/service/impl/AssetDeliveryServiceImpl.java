/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.service.impl;

import static com.pearson.glp.lad.constants.LadConstants.LAD_LEARNING_EXPERIENCE_READY;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pearson.glp.core.handlers.base.ServiceHandlerContext;
import com.pearson.glp.core.handlers.base.ServiceHandlerResponse;
import com.pearson.glp.core.handlers.isc.IscServiceCompensationHandlerContext;
import com.pearson.glp.core.handlers.isc.IscServiceHandlerContext;
import com.pearson.glp.core.handlers.isc.IscServiceHandlerVoidResponse;
import com.pearson.glp.core.handlers.isc.IscServiceRetryHandlerContext;
import com.pearson.glp.core.handlers.rest.JsonPayloadServiceResponse;
import com.pearson.glp.crosscutting.isc.client.async.service.IscAsyncClient;
import com.pearson.glp.lad.dataaccess.AssetDeliveryDataAccess;
import com.pearson.glp.lad.event.model.Payload;
import com.pearson.glp.lad.exceptions.InvalidRequestException;
import com.pearson.glp.lad.service.AssetDeliveryService;
import com.pearson.glp.lad.utils.ValidationUtil;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * The Class AssetDeliveryServiceImpl.
 *
 * @author kaushal.singh1
 */
@Component

/** The Constant log. */
@Slf4j
public class AssetDeliveryServiceImpl implements AssetDeliveryService {

  /** The learning asset delivery data access impl. */
  @Autowired
  private AssetDeliveryDataAccess learningAssetDeliveryDataAccessImpl;

  /** The validation util. */
  @Autowired
  private ValidationUtil validationUtil;

  /** The client. */
  @Autowired
  private IscAsyncClient client;

  /** Sample LAD READY event. **/
  private String sampleReadyEvent = "{ \"id\": \"7f7a82de-babb-4dd9-98c4-4c318c7bc218\", \"timestamp\": 1533677509602, \"eventName\": \"LAD_LEARNING_EXPERIENCE_READY\",\"payload\": \"{}\", \"crosscuttingEvent\": { \"xCorrelationId\": \"0b4b238a-1f4c-44c8-9c1b-cc5752d87ff1\", \"currentCorrelationId\": null, \"correlationId\": null }, \"sagaHeader\": null }";

  /**
   * Fetch Learning Experience.
   *
   * @param context
   *          the context
   * @return the mono
   */
  public Mono<ServiceHandlerResponse> fetchLearningExperiences(ServiceHandlerContext context) {
    log.debug("Service started to fetch Learing Experience");
    String leId = context.getParameter("leId");
    return learningAssetDeliveryDataAccessImpl.fetchLearningExperiences(leId)
        .flatMap(learningexperience -> {
          log.info("Successful fetch for : {}", learningexperience.getLeId());
          return JsonPayloadServiceResponse.ok().setPayload(learningexperience);
        }).switchIfEmpty(validationUtil.emptyResponse()).onErrorResume(x -> {
          if (x instanceof InvalidRequestException) {
            return JsonPayloadServiceResponse.withStatus(HttpStatus.BAD_REQUEST.value())
                .setPayload(validationUtil.prepareResponse400());
          } else {
            return JsonPayloadServiceResponse.withStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .setPayload(validationUtil.prepareServerErrorResponse());
          }

        });
  }

  /**
   * Lec Pre Compose Event Service Handler.
   *
   * @param context
   *          the context
   * @return the mono
   */
  public Mono<IscServiceHandlerVoidResponse> lecPreComposeEventServiceHandler(
      IscServiceHandlerContext context) {
    log.info(" Pre_Compose event Service Handler :{} ", context.getEvent());
    String payloadString = context.getEvent().getPayload();

    Mono.just(payloadString).map(ps -> {
      log.info("Created publisher with payload string");
      ObjectMapper mapper = new ObjectMapper();
      Payload payload = null;
      try {
        payload = mapper.readValue(ps, Payload.class);
      } catch (IOException e) {
        log.error("Exception occurred while reading payload from event", e);
      }
      return payload.getEmbedded();
    }).flatMap(le -> {
      log.info("Retrieved data from PRE_COMPOSE Event for :{}", le.getLeId());
      return learningAssetDeliveryDataAccessImpl.saveLearningExperiences(le);
    }).subscribe(le -> {
      log.info("Persisted Successfully");
      client.publishEvent(LAD_LEARNING_EXPERIENCE_READY, sampleReadyEvent);
    });

    return IscServiceHandlerVoidResponse.ok().build();
  }

  /**
   * Lec Pre Compose Event Service Compensation Handler.
   *
   * @param context
   *          the context
   * @return the mono
   */
  public Mono<IscServiceHandlerVoidResponse> lecPreComposeEventServiceCompensationHandler(
      IscServiceCompensationHandlerContext context) {
    log.info(" Service Compensation Handler [Control present: {} , Event present: {}",
        context.isSagaControlPresent(), context.isEventPresent());
    return IscServiceHandlerVoidResponse.ok().build();
  }

  /**
   * Lec Pre Compose Event Service Retry Handler.
   *
   * @param context
   *          the context
   * @return the mono
   */
  public Mono<IscServiceHandlerVoidResponse> lecPreComposeEventServiceRetryHandler(
      IscServiceRetryHandlerContext context) {
    log.info(" Service Retry Handler [Control present: {} , Event present: {}",
        context.isSagaControlPresent(), context.isEventPresent());
    return IscServiceHandlerVoidResponse.ok().build();
  }
}