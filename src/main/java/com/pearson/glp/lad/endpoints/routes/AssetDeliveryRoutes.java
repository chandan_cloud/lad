/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.endpoints.routes;

import static com.pearson.glp.lad.constants.LadConstants.GET_LEARNING_EXPERIENCE;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;
import static org.springframework.web.reactive.function.server.RequestPredicates.path;
import static org.springframework.web.reactive.function.server.RouterFunctions.nest;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import com.pearson.glp.core.handlers.base.ServiceHandlerManager;
import com.pearson.glp.crosscutting.isc.client.async.service.IscAsyncClient;
import com.pearson.glp.lad.constants.LadConstants;
import com.pearson.glp.lad.data.repository.AssetDeliveryRepository;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * This class is to map routes to their handlers.
 * 
 * @author kaushal.singh1
 *
 */
@Configuration

/** The Constant log. */
@Slf4j
public class AssetDeliveryRoutes {

  /** The learning asset delivery repository. */
  @Autowired
  public AssetDeliveryRepository learningAssetDeliveryRepository;

  /** The handler manager. */
  @Autowired
  private ServiceHandlerManager handlerManager;

  /** The client. */
  @Autowired
  private IscAsyncClient client;

  /**
   * Instantiates a new asset delivery routes.
   */
  public AssetDeliveryRoutes() {
    super();
  }

  /** The path. */
  @Value("${lad.context.path}")
  private String path;

  /** The learning experience url. */
  @Value("${lad.learningexperience.url}")
  private String learningExperienceUrl;

  /** The event. */
  @Value("${lad.testevent.url}")
  private String event;

  /**
   * This is Sample LEC PRE_COMPOSE Event. This need to be removed once
   * Integration with LEC is done.
   **/
  private String sampleEvent = "{ \"id\": \"7f7a82de-babb-4dd9-98c4-4c318c7bc218\", \"timestamp\": 1533677509602, \"eventName\": \"LEC_LEARNING_EXPERIENCE_PRE_COMPOSED\", \"payload\": \"{ \\\"_embedded\\\": { \\\"_id\\\": \\\"98757a5d-c6c0-4d38-991b-dbea95e1a181\\\", \\\"_bssVer\\\": 1, \\\"_ver\\\": \\\"e291a2e7-1ac1-4e22-88f3-15b7f6887369\\\", \\\"_created\\\": \\\"2018-05-18T19:16:15+00:00\\\", \\\"_lastModified\\\": \\\"2018-05-18T19:16:15+00:00\\\", \\\"_docType\\\": \\\"LEARNINGENGAGEMENT\\\", \\\"_assetType\\\": \\\"LEARNINGEXPERIENCE\\\", \\\"assetClass\\\": \\\"COURSE\\\", \\\"label\\\": \\\"REVEL\\\", \\\"resources\\\": { \\\"COURSETYPE\\\": { \\\"_resourceType\\\": \\\"INLINED\\\", \\\"category\\\": \\\"narrative\\\", \\\"data\\\": \\\"REVEL\\\", \\\"model\\\": \\\"COURSETYPE\\\" }, \\\"COURSETITLE\\\": { \\\"_resourceType\\\": \\\"INLINED\\\", \\\"category\\\": \\\"narrative\\\", \\\"data\\\": \\\"Psychology\\\", \\\"model\\\": \\\"COURSETITLE\\\" }, \\\"COURSEAVATAR\\\": { \\\"_resourceType\\\": \\\"INLINED\\\", \\\"category\\\": \\\"image\\\", \\\"data\\\": \\\"http://url\\\", \\\"model\\\": \\\"COURSEAVATAR\\\" } }, \\\"assetGraph\\\": [], \\\"resourcePlan\\\": [ { \\\"label\\\": \\\"****Selected by the Instructor****\\\", \\\"resourceElementType\\\": \\\"INLINED\\\", \\\"resourceRef\\\": \\\"d441ee2a-4475-4511-969c-80cbbeba553e\\\", \\\"resourceElements\\\": [] }, { \\\"label\\\": \\\"****Selected by the Instructor****\\\", \\\"resourceElementType\\\": \\\"INLINED\\\", \\\"resourceRef\\\": \\\"f9d0b79f-2177-47ac-86f4-ea1971fa5754\\\", \\\"resourceElements\\\": [] }, { \\\"label\\\": \\\"****Selected by the Instructor****\\\", \\\"resourceElementType\\\": \\\"INLINED\\\", \\\"resourceRef\\\": \\\"3284469b-b951-49dd-95fe-cf82a9364766\\\", \\\"resourceElements\\\": [] } ], \\\"configuration\\\": { \\\"experienceComposition\\\": {}, \\\"experienceManagement\\\": {}, \\\"assetDelivery\\\": { \\\"supportedComponents\\\": [ \\\"glp-assessment-mcqsa\\\", \\\"glp-assessment-mcqma\\\", \\\"glp-assessment-fib\\\", \\\"glp-assessment-mcqsa-fib\\\" ] }, \\\"assetEvaluation:\\\": { \\\"scoringPolicy\\\": [ { \\\"attributes\\\": [ \\\"noOfAttempts\\\", \\\"noOfTries\\\" ], \\\"attributesValue\\\": { \\\"maxScorePerAssessment\\\": \\\"1\\\", \\\"negativeMarks\\\": \\\"0\\\", \\\"noOfAttemptAllowed\\\": \\\"1\\\", \\\"noOfTriesAllowed\\\": \\\"3\\\", \\\"numberOfPart\\\": \\\"1.0\\\" }, \\\"desc\\\": \\\"\\\", \\\"evalExpression\\\": { \\\"score\\\": \\\"(maxScorePerAssessment - ((noOfAttempts-1)*negativeMarks))/numberOfPart\\\" }, \\\"masterAttributes\\\": [ \\\"maxScorePerAssessment\\\", \\\"negativeMarks\\\", \\\"noOfAttemptAllowed\\\", \\\"noOfTriesAllowed\\\", \\\"numberOfPart\\\" ], \\\"strategy\\\": \\\"FORMATIVE\\\" }, { \\\"attributes\\\": [ \\\"noOfAttempts\\\", \\\"noOfTries\\\" ], \\\"attributesValue\\\": { \\\"maxScorePerAssessment\\\": \\\"1\\\", \\\"negativeMarks\\\": \\\"0\\\", \\\"noOfAttemptAllowed\\\": \\\"1\\\", \\\"noOfTriesAllowed\\\": \\\"1\\\", \\\"numberOfPart\\\": \\\"1.0\\\" }, \\\"desc\\\": \\\"\\\", \\\"evalExpression\\\": { \\\"score\\\": \\\"(maxScorePerAssessment - ((noOfAttempts-1)*negativeMarks))/numberOfPart\\\" }, \\\"masterAttributes\\\": [ \\\"maxScorePerAssessment\\\", \\\"negativeMarks\\\", \\\"noOfAttemptAllowed\\\", \\\"noOfTriesAllowed\\\", \\\"numberOfPart\\\" ], \\\"strategy\\\": \\\"SUMMATIVE\\\" } ] }, \\\"adaptive\\\": { \\\"coreInstructions\\\": \\\"false\\\", \\\"isOverridable\\\": \\\"true\\\", \\\"maxQuestions\\\": { \\\"postAssessment\\\": \\\"12\\\", \\\"practiceAssessment\\\": \\\"12\\\", \\\"preAssessment\\\": \\\"20\\\" }, \\\"postAssessment\\\": \\\"false\\\", \\\"practiceAssessment\\\": \\\"true\\\", \\\"preAssessment\\\": \\\"true\\\", \\\"type\\\": \\\"DYNAMIC\\\", \\\"courseMastery\\\": { \\\"PostAssessment\\\": { \\\"lowerBound\\\": 80, \\\"mastery\\\": 90, \\\"upperBound\\\": 100 }, \\\"Practice\\\": { \\\"lowerBound\\\": 80, \\\"mastery\\\": 90, \\\"upperBound\\\": 100 }, \\\"PreAssessment\\\": { \\\"lowerBound\\\": 80, \\\"mastery\\\": 90, \\\"upperBound\\\": 100 } } } }, \\\"constraints\\\": [], \\\"extends\\\": { \\\"sources\\\": [ { \\\"_resourceType\\\": \\\"LEARNINGASSET\\\", \\\"_docType\\\": \\\"LEARNINGCONTENT\\\", \\\"_assetType\\\": \\\"PRODUCT\\\", \\\"_id\\\": \\\"5ca1279d-7b8a-471f-aa98-8574c608288a\\\", \\\"_bssVer\\\": 1, \\\"_ver\\\": \\\"b9b308e2-9b3e-4d66-9572-98045cffd5ed\\\", \\\"_links\\\": { \\\"self\\\": { \\\"href\\\": \\\"/v1/products/5ca1279d-7b8a-471f-aa98-8574c608288a?_bssVer=1\\\" } } } ] }, \\\"extensions\\\": { \\\"sectionId\\\": \\\"12564\\\", \\\"sectionCode\\\": \\\"123213\\\", \\\"externalId\\\": \\\"4324\\\", \\\"organizationId\\\": \\\"42323\\\" }, \\\"scope\\\": {}, \\\"learningModel\\\": { \\\"_resourceType\\\": \\\"LEARNINGASSET\\\", \\\"_docType\\\": \\\"LEARNINGMODEL\\\", \\\"_assetType\\\": \\\"LEARNINGEXPERIENCE\\\", \\\"_id\\\": \\\"32f42ce8-61dd-4bbc-bba5-1fdc03a0af9f\\\", \\\"_bssVer\\\": 1, \\\"_ver\\\": \\\"810a3768-17af-4f2f-9d4c-b07c6cdfc672\\\", \\\"_links\\\": { \\\"self\\\": { \\\"href\\\": \\\"/v1/models/32f42ce8-61dd-4bbc-bba5-1fdc03a0af9f?_bssVer=1\\\" } } }, \\\"_links\\\": { \\\"self\\\": { \\\"href\\\": \\\"/v1/learningExperiences/68757a5d-c6c0-4d38-991b-dbea95e1a18b\\\" }, \\\"status\\\": { \\\"href\\\": \\\"/v1/learningExperiences/68757a5d-c6c0-4d38-991b-dbea95e1a18b/status\\\" }, \\\"version\\\": { \\\"href\\\": \\\"/v1/learningExperiences/68757a5d-c6c0-4d38-991b-dbea95e1a18b/versions/e291a2e7-1ac1-4e22-88f3-15b7f6887369\\\" } } } }\", \"crosscuttingEvent\": { \"xCorrelationId\": \"0b4b238a-1f4c-44c8-9c1b-cc5752d87ff1\", \"currentCorrelationId\": null, \"correlationId\": null }, \"sagaHeader\": null }";

  /**
   * Inits the custom routes.
   *
   * @return the router function
   */
  @Bean
  @DependsOn("LearningAssetDeliveryHandlers")
  public RouterFunction<ServerResponse> initAssetDeliveryRoutes() {
    log.info("Registering routes to handler");
    return nest(path(path), nest(accept(MediaType.APPLICATION_JSON),
        route(GET(learningExperienceUrl), handlerManager.getRestHandler(GET_LEARNING_EXPERIENCE))
            .andRoute(GET(event), request -> {
              Mono<Boolean> publisher = client
                  .publishEvent(LadConstants.LEC_LEARNING_EXPERIENCE_PRE_COMPOSED, sampleEvent);
              return ServerResponse.ok().body(publisher, Boolean.class);
            })));
  }
}