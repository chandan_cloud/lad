/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.handlers;

import static com.pearson.glp.lad.constants.LadConstants.*;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pearson.glp.core.handlers.base.ServiceHandlerManager;
import com.pearson.glp.lad.service.AssetDeliveryService;

import lombok.extern.slf4j.Slf4j;

/**
 * The Class AssetDeliveryHandlers.
 */
@Component("LearningAssetDeliveryHandlers")

/** The Constant log. */
@Slf4j
public class AssetDeliveryHandlers {

  /** The learning asset delivery service. */
  @Autowired
  private AssetDeliveryService learningAssetDeliveryService;

  /** The handler manager. */
  @Autowired
  private ServiceHandlerManager handlerManager;

  /**
   * Register default handlers.
   */
  @PostConstruct
  public void registerDefaultHandlers() {
    log.info("Registering Handlers");
    handlerManager.registerHandler(GET_LEARNING_EXPERIENCE,
        learningAssetDeliveryService::fetchLearningExperiences);
    log.info("Handlers Registered");
  }

}
