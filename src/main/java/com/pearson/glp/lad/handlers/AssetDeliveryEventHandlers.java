/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.handlers;

import static com.pearson.glp.lad.constants.LadConstants.ISC_HANDLERS;
import static com.pearson.glp.lad.constants.LadConstants.LEC_LEARNING_EXPERIENCE_PRE_COMPOSED;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.pearson.glp.core.handlers.base.ServiceHandlerManager;
import com.pearson.glp.crosscutting.isc.client.async.model.EventMessage;
import com.pearson.glp.lad.events.Events;
import com.pearson.glp.lad.service.AssetDeliveryService;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

/**
 * The Class AssetDeliveryEventHandlers.
 */
@Component(ISC_HANDLERS + LEC_LEARNING_EXPERIENCE_PRE_COMPOSED)
@DependsOn("serviceHandlerManager")
@EnableBinding(Events.class)

/** The Constant log. */
@Slf4j
public class AssetDeliveryEventHandlers {

  /** The handlers manager. */
  @Autowired
  private ServiceHandlerManager handlersManager;

  /** The learning asset delivery service. */
  @Autowired
  private AssetDeliveryService learningAssetDeliveryService;

  /**
   * Input handler.
   *
   * @param incomingEvent
   *          the incoming event
   */
  @StreamListener
  public void inputHandler(
      @Input(LEC_LEARNING_EXPERIENCE_PRE_COMPOSED) Flux<EventMessage> incomingEvent) {
    log.info("Registering Event Handlers");
    handlersManager.registerIscHandlers(LEC_LEARNING_EXPERIENCE_PRE_COMPOSED, incomingEvent,
        learningAssetDeliveryService::lecPreComposeEventServiceHandler,
        learningAssetDeliveryService::lecPreComposeEventServiceCompensationHandler,
        learningAssetDeliveryService::lecPreComposeEventServiceRetryHandler);

  }
}
