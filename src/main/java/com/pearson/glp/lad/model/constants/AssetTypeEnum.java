/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.model.constants;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

/**
 * The Enum AssetTypeEnum.
 */
public enum AssetTypeEnum {

	/** The narrative. */
	NARRATIVE,
	/** The assessment item. */
	@JsonProperty("ASSESSMENT-ITEM")
	@SerializedName("ASSESSMENT-ITEM")
	ASSESSMENT_ITEM,
	/** The instruction. */
	INSTRUCTION,
	/** The assessment. */
	ASSESSMENT,
	/** The tool. */
	TOOL,
	/** The aggregate. */
	AGGREGATE,
	/** The product. */
	PRODUCT,
	/** The learningexperience. */
	LEARNINGEXPERIENCE,
	/** The engagementplan. */
	ENGAGEMENTPLAN,
	/** The userengagement. */
	USERENGAGEMENT

}
