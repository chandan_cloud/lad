/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.pearson.glp.lad.model.constants.AssetTypeEnum;
import com.pearson.glp.lad.model.constants.DocumentTypeEnum;
import com.pearson.glp.lad.model.constants.ResourceTypeEnum;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The Class AssetRelationship.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AssetRelationship extends ImmutableEntityVersionIdentifier implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5897754932536175698L;

  /** The resource type. */
  @SerializedName("_resourceType")
  @JsonProperty("_resourceType")
  @Field("_resourceType")
  private ResourceTypeEnum resourceType;

  /** The doc type. */
  @SerializedName("_docType")
  @JsonProperty("_docType")
  @Field("_docType")
  private DocumentTypeEnum docType;

  /** The asset type. */
  @SerializedName("_assetType")
  @JsonProperty("_assetType")
  @Field("_assetType")
  private AssetTypeEnum assetType;

  /** The links. */
  @SerializedName("_links")
  @JsonProperty("_links")
  @Field("_links")
  private Map<String, Link> links = new HashMap<>();

}
