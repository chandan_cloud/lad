/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;

import lombok.Data;

/**
 * The Class AssetGraphItem.
 */
@Data
public class AssetGraphItem implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8478626221980081365L;

  /** The start node. */
  private String startNode;

  /** The end node. */
  private String endNode;

  /** The relationships. */
  private Relationship relationships;
}
