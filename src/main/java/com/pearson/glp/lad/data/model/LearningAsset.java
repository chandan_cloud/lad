/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.pearson.glp.lad.model.constants.AssetClassEnum;
import com.pearson.glp.lad.model.constants.AssetTypeEnum;
import com.pearson.glp.lad.model.constants.DocumentTypeEnum;
import com.pearson.glp.lad.model.constants.ProductModelEnum;

import lombok.Data;

/**
 * Instantiates a new learning asset.
 */
@Data
public class LearningAsset implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6451193291215128732L;

  /** The created. */
  @SerializedName("_created")
  @JsonProperty("_created")
  @Field("_created")
  private String created;

  /** The last modified. */
  @SerializedName("_lastModified")
  @JsonProperty("_lastModified")
  @Field("_lastModified")
  private String lastModified;

  /** The expires on. */
  private String expiresOn;

  /** The label. */
  private ProductModelEnum label;

  /** The tags. */
  private String tags;

  /** The language. */
  private String language;

  /** The doc type. */
  @JsonProperty("_docType")
  @SerializedName("_docType")
  @Field("_docType")
  private DocumentTypeEnum docType;

  /** The asset type. */
  @SerializedName("_assetType")
  @JsonProperty("_assetType")
  @Field("_assetType")
  private AssetTypeEnum assetType;

  /** The asset class. */
  private AssetClassEnum assetClass;

  /** The objectives. */
  private String objectives;

  /** The groups. */
  private Group groups;

  /** The resources. */
  private AssetResourceArray resources;

  /** The learning model. */
  private AssetRelationship learningModel;

  /** The asset graph. */
  private List<AssetGraphItem> assetGraph;

  /** The resource plan. */
  private List<ResourcePlanItem> resourcePlan;

  /** The configuration. */
  private Configuration configuration;

  /** The constraints. */
  private List<String> constraints;

  /** The extends. */
  @SerializedName("extends")
  @JsonProperty("extends")
  @Field("extends")
  private Extends extendz;

  /** The extensions. */
  private Map<String, String> extensions;

  /** The scope. */
  private Scope scope;

  /** The links. */
  @SerializedName("_links")
  @JsonProperty("_links")
  @Field("_links")
  private Map<String, Link> links = new HashMap<>();
}
