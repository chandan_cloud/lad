/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;
import java.util.List;

import com.pearson.glp.lad.model.constants.StrategyEnum;

import lombok.Data;

/**
 * Instantiates a new scoring policy.
 */
@Data
public class ScoringPolicy implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -43617874927357956L;

  /** The attributes. */
  private List<String> attributes;

  /** The attributes value. */
  private AttributesValue attributesValue;

  /** The desc. */
  private String desc;

  /** The eval expression. */
  private EvalExpression evalExpression;

  /** The master attributes. */
  private List<String> masterAttributes;

  /** The strategy. */
  private StrategyEnum strategy;

}
