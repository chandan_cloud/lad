/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;

import com.pearson.glp.lad.model.constants.AdaptiveTypeEnum;

import lombok.Data;

/**
 * Instantiates a new adaptive.
 */

@Data
public class Adaptive implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 117449297317276963L;

  /**
   * Instantiates a new adaptive.
   */
  public Adaptive() {
    super();
  }

  /** The core instructions. */
  private Boolean coreInstructions;

  /** The is overridable. */
  private Boolean isOverridable;

  /** The post assessment. */
  private Boolean postAssessment;

  /** The practice assessment. */
  private Boolean practiceAssessment;

  /** The pre assessment. */
  private Boolean preAssessment;

  /** The type. */
  private AdaptiveTypeEnum type;

  /** The max questions. */
  private MaxQuestion maxQuestions;

  /** The course mastery. */
  private CourseMastery courseMastery;
}
