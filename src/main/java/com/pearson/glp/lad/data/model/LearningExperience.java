/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import org.springframework.data.couchbase.core.mapping.Document;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The Class LearningExperience.
 */
@Document
@Data
@EqualsAndHashCode(callSuper = true)
public class LearningExperience extends LearningAsset {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2803775612820335333L;

  /** The id. */
  @SerializedName("_id")
  @JsonProperty("_id")
  @Field("_id")
  @Id
  private String leId;

  /** The bss ver. */
  @SerializedName("_bssVer")
  @JsonProperty("_bssVer")
  @Field("_bssVer")
  private Integer bssVer;

  /** The ver. */
  @SerializedName("_ver")
  @JsonProperty("_ver")
  @Field("_ver")
  private String ver;
}
