/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * The Class ResourcePlanItem.
 */
@Data
public class ResourcePlanItem implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1050736340835676959L;

  /** The label. */
  private String label;

  /** The resource element type. */
  private String resourceElementType;

  /** The resource ref. */
  private String resourceRef;

  /** The resource elements. */
  private List<ResourcePlanItem> resourceElements = new ArrayList<>();
}
