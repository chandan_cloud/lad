/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * Instantiates a new course mastery.
 */
@Data
public class CourseMastery implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6356519067631815412L;

  /** The pre assessment. */
  @JsonProperty("PreAssessment")
  @SerializedName("PreAssessment")
  @Field("PreAssessment")
  private CourseMasteryLevel preAssessment;

  /** The practice. */
  @JsonProperty("Practice")
  @SerializedName("Practice")
  @Field("Practice")
  private CourseMasteryLevel practice;

  /** The post assessment. */
  @JsonProperty("PostAssessment")
  @SerializedName("PostAssessment")
  @Field("PostAssessment")
  private CourseMasteryLevel postAssessment;
}
