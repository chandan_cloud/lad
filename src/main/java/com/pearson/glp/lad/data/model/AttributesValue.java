/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;

import lombok.Data;

/**
 * Instantiates a new attributes value.
 */
@Data
public class AttributesValue implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5201824395640366895L;

  /** The max score per assessment. */
  private String maxScorePerAssessment;

  /** The negative marks. */
  private String negativeMarks;

  /** The no of attempt allowed. */
  private String noOfAttemptAllowed;

  /** The no of tries allowed. */
  private String noOfTriesAllowed;

  /** The number of part. */
  private String numberOfPart;

}
