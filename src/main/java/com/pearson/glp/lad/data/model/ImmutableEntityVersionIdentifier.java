/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * The Class ImmutableEntityVersionIdentifier.
 */
@Data
public class ImmutableEntityVersionIdentifier implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 370971609004316649L;

  /** The id. */
  @JsonProperty("_id")
  @SerializedName("_id")
  @Field("_id")
  private String id;

  /** The bss ver. */
  @SerializedName("_bssVer")
  @JsonProperty("_bssVer")
  @Field("_bssVer")
  private Integer bssVer;

  /** The ver. */
  @SerializedName("_ver")
  @JsonProperty("_ver")
  @Field("_ver")
  private String ver;

}
