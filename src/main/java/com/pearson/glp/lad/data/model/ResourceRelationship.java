/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.pearson.glp.lad.model.constants.ResourceTypeEnum;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Instantiates a new resource relationship.
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ResourceRelationship extends ImmutableEntityVersionIdentifier {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8661367859590394334L;

  /** The resource type. */
  @JsonProperty("_resourceType")
  @SerializedName("_resourceType")
  @Field("_resourceType")
  private ResourceTypeEnum resourceType;

}
