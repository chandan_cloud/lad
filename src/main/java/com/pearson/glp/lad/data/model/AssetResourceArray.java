/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.Serializable;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

/**
 * The Class AssetResourceArray.
 */
@Data
public class AssetResourceArray implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5658249797481845644L;

  /** The course type. */
  @JsonProperty("COURSETYPE")
  @SerializedName("COURSETYPE")
  @Field("COURSETYPE")
  private AssetInlinedResource courseType;

  /** The course title. */
  @JsonProperty("COURSETITLE")
  @SerializedName("COURSETITLE")
  @Field("COURSETITLE")
  private AssetInlinedResource courseTitle;

  /** The course avatar. */
  @JsonProperty("COURSEAVATAR")
  @SerializedName("COURSEAVATAR")
  @Field("COURSEAVATAR")
  private AssetInlinedResource courseAvatar;

}
