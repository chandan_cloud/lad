/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.data.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.couchbase.client.java.repository.annotation.Field;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.pearson.glp.lad.model.constants.ContentCategoryEnum;
import com.pearson.glp.lad.model.constants.ResourceTypeEnum;

import lombok.Data;

/**
 * Instantiates a new asset inlined resource.
 */
/**
 * @author kaushal.singh1
 *
 */

@Data
public class AssetInlinedResource implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6771391429660477834L;

  /** The resource type. */
  @SerializedName("_resourceType")
  @JsonProperty("_resourceType")
  @Field("_resourceType")
  private ResourceTypeEnum resourceType;

  /** The category. */
  private ContentCategoryEnum category;

  /** The model. */
  private String model;

  /** The data. */
  private Object data;

  /**
   * Write object.
   *
   * @param out
   *          the out
   * @throws IOException
   *           Signals that an I/O exception has occurred.
   */
  private void writeObject(ObjectOutputStream out) throws IOException {
    out.defaultWriteObject();
    out.writeObject(this.data);
  }

  /**
   * Read object.
   *
   * @param in
   *          the in
   * @throws IOException
   *           Signals that an I/O exception has occurred.
   * @throws ClassNotFoundException
   *           the class not found exception
   */
  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    this.data = in.readObject();
  }
}
