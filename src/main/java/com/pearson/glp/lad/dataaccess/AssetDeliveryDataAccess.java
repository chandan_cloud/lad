/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.dataaccess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pearson.glp.lad.data.model.LearningExperience;
import com.pearson.glp.lad.data.repository.AssetDeliveryRepository;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

/**
 * The Class AssetDeliveryDataAccess.
 */
@Component

/** The Constant log. */
@Slf4j
public class AssetDeliveryDataAccess {

  /** The learning asset delivery repository. */
  @Autowired
  private AssetDeliveryRepository learningAssetDeliveryRepository;

  /**
   * Instantiates a new asset delivery data access.
   */
  public AssetDeliveryDataAccess() {
    super();
  }

  /**
   * Fetch learning experiences.
   *
   * @param leid
   *          the leid
   * @return the mono
   */
  public Mono<LearningExperience> fetchLearningExperiences(String leid) {
    log.info("Fetch Learning Experience for {}", leid);
    return learningAssetDeliveryRepository.findById(leid);
  }

  /**
   * Save learning experiences.
   *
   * @param learningExperience
   *          the learning experience
   * @return the mono
   */
  public Mono<LearningExperience> saveLearningExperiences(LearningExperience learningExperience) {
    log.info("Persist Learning Experience {}", learningExperience.getLeId());
    return learningAssetDeliveryRepository.save(learningExperience);

  }
}
