/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.pearson.glp.core.handlers.rest.JsonPayloadServiceResponse;
import com.pearson.glp.lad.data.model.ErrorResponse;

import reactor.core.publisher.Mono;

/**
 * The Class ValidationUtil.
 */
@Component
public class ValidationUtil {

  /** The errorcode 400. */
  @Value("${errorcode.400}")
  private String errorcode400;

  /** The errorcode 404. */
  @Value("${errorcode.404}")
  private String errorcode404;

  /** The errorcode 500. */
  @Value("${errorcode.500}")
  private String errorcode500;

  /**
   * Empty response.
   *
   * @return the mono
   */
  public Mono<JsonPayloadServiceResponse> emptyResponse() {
    JsonPayloadServiceResponse jsonPayloadServiceResponse = new JsonPayloadServiceResponse();

    jsonPayloadServiceResponse.setStatus(HttpStatus.NOT_FOUND.value());
    jsonPayloadServiceResponse.setPayload(prepareResponse404());

    return Mono.just(jsonPayloadServiceResponse);
  }

  /**
   * Prepare response.
   *
   * @param <T>
   *          the generic type
   * @return the mono
   */
  public <T> Mono<T> validateLearningExperienceId() {
    JsonPayloadServiceResponse jsonPayloadServiceResponse = new JsonPayloadServiceResponse();

    jsonPayloadServiceResponse.setStatus(HttpStatus.NOT_FOUND.value());
    jsonPayloadServiceResponse.setPayload(prepareResponse400());

    return (Mono<T>) Mono.just(jsonPayloadServiceResponse);
  }

  /**
   * Prepare response 404.
   *
   * @return the error response
   */
  public ErrorResponse prepareResponse404() {
    ErrorResponse response = new ErrorResponse();
    response.setErrorCode(HttpStatus.NOT_FOUND.value());
    response.setErrorDesc(errorcode404);
    response.set_userDesc(errorcode404);
    return response;
  }

  /**
   * Prepare response 400.
   *
   * @return the error response
   */
  public ErrorResponse prepareResponse400() {
    ErrorResponse response = new ErrorResponse();
    response.setErrorCode(HttpStatus.BAD_REQUEST.value());
    response.setErrorDesc(errorcode400);
    response.set_userDesc(errorcode400);
    return response;
  }

  /**
   * Prepare response 500.
   *
   * @return the error response
   */
  public ErrorResponse prepareServerErrorResponse() {
    ErrorResponse errorResponse = new ErrorResponse();
    errorResponse.setErrorCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
    errorResponse.set_userDesc(errorcode500);
    errorResponse.setErrorDesc(errorcode500);
    return errorResponse;
  }
}
