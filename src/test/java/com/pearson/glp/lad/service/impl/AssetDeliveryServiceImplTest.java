/*
 * PEARSON PROPRIETARY AND CONFIDENTIAL INFORMATION SUBJECT TO NDA 
 * Copyright (c) 2017 Pearson Education, Inc.
 * All Rights Reserved. 
 * 
 * NOTICE: All information contained herein is, and remains the property of 
 * Pearson Education, Inc. The intellectual and technical concepts contained 
 * herein are proprietary to Pearson Education, Inc. and may be covered by U.S. 
 * and Foreign Patents, patent applications, and are protected by trade secret 
 * or copyright law. Dissemination of this information, reproduction of this  
 * material, and copying or distribution of this software is strictly forbidden   
 * unless prior written permission is obtained from Pearson Education, Inc.
 
 */
package com.pearson.glp.lad.service.impl;

import static com.pearson.glp.lad.constants.LadConstants.LAD_LEARNING_EXPERIENCE_READY;
import static com.pearson.glp.lad.constants.LadConstants.LEC_LEARNING_EXPERIENCE_PRE_COMPOSED;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pearson.glp.core.handlers.base.ServiceHandlerContext;
import com.pearson.glp.core.handlers.base.ServiceHandlerResponse;
import com.pearson.glp.core.handlers.isc.IscServiceCompensationHandlerContext;
import com.pearson.glp.core.handlers.isc.IscServiceHandlerContext;
import com.pearson.glp.core.handlers.isc.IscServiceHandlerVoidResponse;
import com.pearson.glp.core.handlers.isc.IscServiceRetryHandlerContext;
import com.pearson.glp.core.handlers.rest.JsonPayloadServiceResponse;
import com.pearson.glp.crosscutting.isc.client.async.model.EventImpl;
import com.pearson.glp.crosscutting.isc.client.async.service.IscAsyncClient;
import com.pearson.glp.crosscutting.isc.client.sync.service.IscSyncClient;
import com.pearson.glp.lad.data.model.Adaptive;
import com.pearson.glp.lad.data.model.AssetGraphItem;
import com.pearson.glp.lad.data.model.AssetInlinedResource;
import com.pearson.glp.lad.data.model.AssetRelationship;
import com.pearson.glp.lad.data.model.AssetResourceArray;
import com.pearson.glp.lad.data.model.Configuration;
import com.pearson.glp.lad.data.model.CourseMastery;
import com.pearson.glp.lad.data.model.CourseMasteryLevel;
import com.pearson.glp.lad.data.model.Engagement;
import com.pearson.glp.lad.data.model.ErrorResponse;
import com.pearson.glp.lad.data.model.Extends;
import com.pearson.glp.lad.data.model.Group;
import com.pearson.glp.lad.data.model.LearningExperience;
import com.pearson.glp.lad.data.model.MaxQuestion;
import com.pearson.glp.lad.data.model.Relationship;
import com.pearson.glp.lad.data.model.ResourcePlanItem;
import com.pearson.glp.lad.data.model.Scope;
import com.pearson.glp.lad.dataaccess.AssetDeliveryDataAccess;
import com.pearson.glp.lad.model.constants.AdaptiveTypeEnum;
import com.pearson.glp.lad.model.constants.AssetClassEnum;
import com.pearson.glp.lad.model.constants.AssetTypeEnum;
import com.pearson.glp.lad.model.constants.ContentCategoryEnum;
import com.pearson.glp.lad.model.constants.DocumentTypeEnum;
import com.pearson.glp.lad.model.constants.ProductModelEnum;
import com.pearson.glp.lad.model.constants.ResourceTypeEnum;
import com.pearson.glp.lad.utils.ValidationUtil;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 * The Class AssetDeliveryServiceImplTest.
 */
public class AssetDeliveryServiceImplTest {

  /** The Constant OK. */
  private static final int OK = 200;

  private static Gson gson = new GsonBuilder().serializeNulls().create();

  /** The learning asset delivery service impl. */
  @InjectMocks
  private AssetDeliveryServiceImpl learningAssetDeliveryServiceImpl;

  /** The context. */
  @Mock
  private ServiceHandlerContext context;

  /** The isc context. */
  @Mock
  private IscServiceHandlerContext iscContext;

  /** The isc compensation context. */
  @Mock
  private IscServiceCompensationHandlerContext iscCompensationContext;

  /** The isc retry context. */
  @Mock
  private IscServiceRetryHandlerContext iscRetryContext;

  /** The learning asset delivery data access impl. */
  @Mock
  private AssetDeliveryDataAccess learningAssetDeliveryDataAccessImpl;

  /** The isc sync client. */
  @Mock
  private IscSyncClient iscSyncClient;

  /** The isc asyncclient. */
  @Mock
  private IscAsyncClient iscAsyncclient;

  /** The validation util. */
  @Mock
  private ValidationUtil validationUtil;

  /** The events. */
  @Mock
  private EventImpl events;

  /**
   * Instantiates a new asset delivery service impl test.
   */
  public AssetDeliveryServiceImplTest() {
    super();
  }

  /**
   * Before method.
   */
  @Before
  public void beforeMethod() {
    MockitoAnnotations.initMocks(this);
  }

  /**
   * Test fetch learning experiences.
   */
  @Test
  public void testFetchLearningExperiences() {
    when(context.getParameter("leId")).thenReturn("68757a5d-c6c0-4d38-991b-dbea95e1a18b");

    Mono<LearningExperience> le = getMonoLearningExperience();
    when(learningAssetDeliveryDataAccessImpl.fetchLearningExperiences(anyString())).thenReturn(le);
    when(validationUtil.emptyResponse()).thenReturn(emptyResponse());

    StepVerifier.create(learningAssetDeliveryServiceImpl.fetchLearningExperiences(context))
        .assertNext(res -> Assert.assertThat(res.getStatus(), is(OK))).verifyComplete();

  }

  /**
   * Test fetch learning experiences error.
   */
  @Test(expected = NullPointerException.class)
  public void testFetchLearningExperiencesError() {

    when(context.getParameter("leId")).thenReturn("5fbff637-50cc-4843-9c55-0afc1f9fc464");

    when(learningAssetDeliveryDataAccessImpl.fetchLearningExperiences(anyString()))
        .thenReturn(null);

    when(validationUtil.emptyResponse()).thenReturn(emptyResponse());

    StepVerifier.create(learningAssetDeliveryServiceImpl.fetchLearningExperiences(context))
        .verifyError(NullPointerException.class);
  }

  /**
   * Test fetch learning experiences when not found.
   */
  @Test
  public void testFetchLearningExperiencesWhenNotFound() {
    String leId = "68757a5d-c6c0-4d38-991b-dbea95e1a18b";

    when(context.getParameter("leId")).thenReturn(leId);

    when(learningAssetDeliveryDataAccessImpl.fetchLearningExperiences(leId))
        .thenReturn(Mono.empty());

    when(validationUtil.emptyResponse()).thenReturn(emptyResponse());

    StepVerifier.create(learningAssetDeliveryServiceImpl.fetchLearningExperiences(context))
        .assertNext(res -> Assert.assertThat(res.getStatus(), is(HttpStatus.NOT_FOUND.value())))
        .verifyComplete();

  }

  /**
   * Test save learning experiences.
   *
   * @throws Exception
   *           the exception
   */
  @Test
  public void testSaveLearningExperiences() {

    String payloadString = "{\"_embedded\":{ \"assetGraph\": [], \"resourcePlan\": [ { \"resourceRef\": \"d441ee2a-4475-4511-969c-80cbbeba553e\", \"resourceElementType\": \"INLINED\", \"resourceElements\": [], \"label\": \"RIO\" }, { \"resourceRef\": \"f9d0b79f-2177-47ac-86f4-ea1971fa5754\", \"resourceElementType\": \"INLINED\", \"resourceElements\": [], \"label\": \"****Selected by the Instructor****\" } ], \"configuration\": { \"engagement\": { \"adaptive\": { \"coreInstructions\": false, \"maxQuestions\": { \"postAssessment\": 12, \"practiceAssessment\": 12, \"preAssessment\": 20 }, \"isOverridable\": true, \"postAssessment\": false, \"practiceAssessment\": true, \"preAssessment\": true, \"type\": \"DYNAMIC\", \"courseMastery\": { \"PostAssessment\": { \"upperBound\": 100, \"lowerBound\": 80, \"mastery\": 90 }, \"PreAssessment\": { \"upperBound\": 100, \"lowerBound\": 80, \"mastery\": 90 }, \"Practice\": { \"upperBound\": 100, \"lowerBound\": 80, \"mastery\": 90 } } }, \"experienceEngagement\": {}, \"experienceManagement\": {}, \"assetEvaluation\": { \"scoringPolicy\": [ { \"masterAttributes\": [ \"maxScorePerAssessment\", \"negativeMarks\", \"noOfAttemptAllowed\", \"noOfTriesAllowed\", \"numberOfPart\" ], \"attributesValue\": { \"noOfAttemptAllowed\": \"1\", \"noOfTriesAllowed\": \"3\", \"numberOfPart\": \"1.0\", \"maxScorePerAssessment\": \"1\", \"negativeMarks\": \"0\" }, \"attributes\": [ \"noOfAttempts\", \"noOfTries\" ], \"evalExpression\": { \"score\": \"(maxScorePerAssessment - ((noOfAttempts-1)*negativeMarks))/numberOfPart\" }, \"strategy\": \"FORMATIVE\", \"desc\": \"\" }, { \"masterAttributes\": [ \"maxScorePerAssessment\", \"negativeMarks\", \"noOfAttemptAllowed\", \"noOfTriesAllowed\", \"numberOfPart\" ], \"attributesValue\": { \"noOfAttemptAllowed\": \"1\", \"noOfTriesAllowed\": \"1\", \"numberOfPart\": \"1.0\", \"maxScorePerAssessment\": \"1\", \"negativeMarks\": \"0\" }, \"attributes\": [ \"noOfAttempts\", \"noOfTries\" ], \"evalExpression\": { \"score\": \"(maxScorePerAssessment - ((noOfAttempts-1)*negativeMarks))/numberOfPart\" }, \"strategy\": \"SUMMATIVE\", \"desc\": \"\" } ] }, \"assetDelivery\": { \"supportedComponents\": [ \"glp-assessment-mcqsa\", \"glp-assessment-mcqma\", \"glp-assessment-fib\", \"glp-assessment-mcqsa-fib\" ] }, \"experienceComposition\": {} } }, \"_links\": { \"self\": { \"href\": \"/v2/learningExperiences/6980995d-e5f3-4374-882d-36abb197c2a9\" }, \"version\": { \"href\": \"/v2/learningExperiences/6980995d-e5f3-4374-882d-36abb197c2a9/versions/88dfa3a1-ed86-4124-84b2-54fce9a4347e\" }, \"status\": { \"href\": \"/v2/learningExperiences/6980995d-e5f3-4374-882d-36abb197c2a9/status\" } }, \"_ver\": \"88dfa3a1-ed86-4124-84b2-54fce9a4347e\", \"groups\": {}, \"resources\": { \"COURSEAVATAR\": { \"_resourceType\": \"INLINED\", \"data\": \"http://url\", \"model\": \"COURSEAVATAR\", \"category\": \"image\" }, \"COURSETYPE\": { \"_resourceType\": \"INLINED\", \"data\": \"REVEL\", \"model\": \"COURSETYPE\", \"category\": \"narrative\" }, \"COURSETITLE\": { \"_resourceType\": \"INLINED\", \"data\": \"Psychology\", \"model\": \"COURSETITLE\", \"category\": \"narrative\" } }, \"language\": \"en\", \"label\": \"REVEL\", \"assetClass\": \"COURSE\", \"_assetType\": \"LEARNINGEXPERIENCE\", \"constraints\": [], \"tags\": \"tags\", \"_docType\": \"LEARNINGENGAGEMENT\", \"extensions\": { \"organizationId\": \"42323\", \"externalId\": \"4324\", \"sectionCode\": \"123213\", \"sectionId\": \"12564\" }, \"extends\": { \"sources\": [ { \"_docType\": \"LEARNINGCONTENT\", \"_resourceType\": \"LEARNINGASSET\", \"_links\": { \"self\": { \"href\": \"/v1/products/5ca1279d-7b8a-471f-aa98-8574c608288a?11\" } }, \"_bssVer\": 1, \"_ver\": \"b9b308e2-9b3e-4d66-9572-98045cffd5ed\", \"_id\": \"5ca1279d-7b8a-471f-aa98-8574c608288a\", \"_assetType\": \"PRODUCT\" } ] }, \"_bssVer\": 1, \"learningModel\": { \"_docType\": \"LEARNINGMODEL\", \"_resourceType\": \"LEARNINGASSET\", \"_links\": { \"self\": { \"href\": \"/v1/models/32f42ce8-61dd-4bbc-bba5-1fdc03a0af9f?_bssVer=1\" } }, \"_bssVer\": 1, \"_ver\": \"810a3768-17af-4f2f-9d4c-b07c6cdfc672\", \"_id\": \"32f42ce8-61dd-4bbc-bba5-1fdc03a0af9f\", \"_assetType\": \"LEARNINGEXPERIENCE\" }, \"_created\": \"2018-05-10T19:16:15+00:00\", \"scope\": {}, \"expiresOn\": \"2018-05-18T19:16:15+00:00\", \"objectives\": \"objectveString\", \"_id\": \"6980995d-e5f3-4374-882d-36abb197c2a9\", \"_lastModified\": \"2018-05-15T19:16:15+00:00\" } }";

    events.setPayload(payloadString);
    when(iscContext.getEvent()).thenReturn(events);
    when(iscContext.getEvent().getPayload()).thenReturn(payloadString);

    when(learningAssetDeliveryDataAccessImpl.saveLearningExperiences(Mockito.any()))
        .thenReturn(getMonoLearningExperience());

    when(iscAsyncclient.publishEvent(LEC_LEARNING_EXPERIENCE_PRE_COMPOSED,
        LAD_LEARNING_EXPERIENCE_READY)).thenReturn(Mono.just(Boolean.TRUE));

    StepVerifier
        .create(learningAssetDeliveryServiceImpl.lecPreComposeEventServiceHandler(iscContext))
        .assertNext(res -> Assert.assertThat(res.getStatus(), is(OK))).verifyComplete();
  }

  /**
   * Lec pre compose event service compensation handler.
   */
  @Test
  public void lecPreComposeEventServiceCompensationHandler() {

    StepVerifier
        .create(learningAssetDeliveryServiceImpl
            .lecPreComposeEventServiceCompensationHandler(iscCompensationContext))
        .assertNext(res -> Assert.assertThat(res.getStatus(), is(OK))).verifyComplete();

  }

  /**
   * Lec pre compose event retry compensation handler.
   */
  @Test
  public void lecPreComposeEventRetryCompensationHandler() {

    StepVerifier
        .create(
            learningAssetDeliveryServiceImpl.lecPreComposeEventServiceRetryHandler(iscRetryContext))
        .assertNext(res -> Assert.assertThat(res.getStatus(), is(OK))).verifyComplete();

  }

  /**
   * Gets the learning experience.
   *
   * @return the learning experience
   */
  private LearningExperience getLearningExperience() {

    LearningExperience learningExperienceResponse = new LearningExperience();

    String uid = "68757a5d-c6c0-4d38-991b-dbea95e1a18b";
    learningExperienceResponse.setLeId(uid);

    learningExperienceResponse.setBssVer(1);
    learningExperienceResponse.setVer("a7bbcd8b-e1e2-48f2-a368-1b49f86e475c");

    learningExperienceResponse.setCreated("2018-05-18T19:16:15+00:00");
    learningExperienceResponse.setLastModified("2018-05-18T19:16:15+00:00");

    learningExperienceResponse.setExpiresOn("2018-05-18T19:16:15+00:00");
    learningExperienceResponse.setLabel(ProductModelEnum.REVEL);

    String dummyString = "string";
    learningExperienceResponse.setTags("tags");
    learningExperienceResponse.setLanguage("en");
    learningExperienceResponse.setDocType(DocumentTypeEnum.LEARNINGASSET);
    learningExperienceResponse.setAssetType(AssetTypeEnum.LEARNINGEXPERIENCE);
    learningExperienceResponse.setObjectives("objectveString");

    Group groups = new Group();
    learningExperienceResponse.setGroups(groups);

    AssetResourceArray resources = new AssetResourceArray();
    AssetInlinedResource assetInlinedResource1 = new AssetInlinedResource();

    assetInlinedResource1.setResourceType(ResourceTypeEnum.INLINED);

    assetInlinedResource1.setCategory(ContentCategoryEnum.NARRATIVE);

    assetInlinedResource1.setModel("COURSEAVATAR");
    assetInlinedResource1.setData("COURSETYPE");

    resources.setCourseType(assetInlinedResource1);
    learningExperienceResponse.setResources(resources);

    AssetRelationship learningModel = new AssetRelationship();
    learningModel.setAssetType(AssetTypeEnum.LEARNINGEXPERIENCE);
    learningModel.setBssVer(learningExperienceResponse.getBssVer());
    learningModel.setResourceType(ResourceTypeEnum.LEARNINGASSET);
    learningModel.setLinks(new HashMap<>());

    List<AssetGraphItem> assetGraph = new ArrayList<>();
    AssetGraphItem assetGraphItem = new AssetGraphItem();
    assetGraphItem.setStartNode(dummyString);
    assetGraphItem.setEndNode(dummyString);
    Relationship relationships = new Relationship(); //
    assetGraphItem.setRelationships(relationships);
    // assetGraph.add(assetGraphItem);

    List<ResourcePlanItem> resourcePlan = new ArrayList<>();
    ResourcePlanItem resourcePlanItem = new ResourcePlanItem();
    resourcePlanItem.setLabel("REVEL");
    resourcePlanItem.setResourceElementType(dummyString);
    resourcePlanItem.setResourceRef(dummyString);
    resourcePlan.add(resourcePlanItem);
    learningExperienceResponse.setResourcePlan(resourcePlan);

    Configuration configuration = new Configuration();
    Engagement engagement = new Engagement();
    Adaptive adaptive = new Adaptive();
    adaptive.setCoreInstructions(Boolean.TRUE);
    CourseMastery courseMastery = new CourseMastery();
    CourseMasteryLevel courseMasteryLevel = new CourseMasteryLevel();
    courseMasteryLevel.setLowerBound(50);
    courseMasteryLevel.setMastery(90);
    courseMasteryLevel.setUpperBound(70);
    courseMastery.setPostAssessment(courseMasteryLevel);
    courseMastery.setPractice(courseMasteryLevel);
    courseMastery.setPreAssessment(courseMasteryLevel);
    adaptive.setCourseMastery(courseMastery);
    adaptive.setIsOverridable(Boolean.FALSE);
    MaxQuestion maxQuestion = new MaxQuestion();
    maxQuestion.setPostAssessment(1);
    maxQuestion.setPracticeAssessment(maxQuestion.getPostAssessment());
    maxQuestion.setPreAssessment(maxQuestion.getPostAssessment());
    adaptive.setMaxQuestions(maxQuestion);
    adaptive.setPostAssessment(Boolean.TRUE);
    adaptive.setPracticeAssessment(Boolean.FALSE);
    adaptive.setPreAssessment(Boolean.FALSE);
    adaptive.setType(AdaptiveTypeEnum.DYNAMIC);
    engagement.setAdaptive(adaptive);
    // configuration.setExperienceEngagement(engagement);
    learningExperienceResponse.setConfiguration(configuration);

    List<String> constraints = Arrays.asList(dummyString, dummyString);
    learningExperienceResponse.setConstraints(constraints);

    Extends _extends = new Extends();
    List<AssetRelationship> sources = new ArrayList<>();
    AssetRelationship e = new AssetRelationship();
    e.setAssetType(AssetTypeEnum.LEARNINGEXPERIENCE);
    e.setDocType(DocumentTypeEnum.LEARNINGASSET);
    e.setResourceType(ResourceTypeEnum.LEARNINGASSET);
    sources.add(e);
    _extends.setSources(sources);
    learningExperienceResponse.setExtendz(_extends);

    Map<String, String> extensions = new HashMap<>();
    learningExperienceResponse.setExtensions(extensions);

    learningExperienceResponse.setAssetClass(AssetClassEnum.COURSE);
    learningExperienceResponse.setAssetGraph(assetGraph);
    learningExperienceResponse.setDocType(DocumentTypeEnum.LEARNINGASSET);

    Scope scope = new Scope();
    learningExperienceResponse.setScope(scope);

    return learningExperienceResponse;
  }

  /**
   * Gets the mono learning experience.
   *
   * @return the mono learning experience
   */
  private Mono<LearningExperience> getMonoLearningExperience() {
    return Mono.just(getLearningExperience());
  }

  /**
   * Empty response.
   *
   * @return the mono
   */
  public Mono<JsonPayloadServiceResponse> emptyResponse() {
    JsonPayloadServiceResponse jsonPayloadServiceResponse = new JsonPayloadServiceResponse();

    jsonPayloadServiceResponse.setStatus(HttpStatus.NOT_FOUND.value());

    jsonPayloadServiceResponse.setPayload(getErrorResponse404());

    return Mono.just(jsonPayloadServiceResponse);
  }

  /**
   * Gets the error response 404.
   *
   * @return the error response 404
   */
  private ErrorResponse getErrorResponse404() {

    ErrorResponse response = new ErrorResponse();
    response.setErrorCode(HttpStatus.NOT_FOUND.value());
    response.setErrorDesc("Learning Experience Not Found.");
    response.set_userDesc("Learning Experience Not Found.");
    return response;
  }
}
