package com.pearson.glp.lad.dataaccess;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.pearson.glp.lad.data.model.Adaptive;
import com.pearson.glp.lad.data.model.AssetGraphItem;
import com.pearson.glp.lad.data.model.AssetInlinedResource;
import com.pearson.glp.lad.data.model.AssetRelationship;
import com.pearson.glp.lad.data.model.AssetResourceArray;
import com.pearson.glp.lad.data.model.Configuration;
import com.pearson.glp.lad.data.model.CourseMastery;
import com.pearson.glp.lad.data.model.CourseMasteryLevel;
import com.pearson.glp.lad.data.model.Engagement;
import com.pearson.glp.lad.data.model.Extends;
import com.pearson.glp.lad.data.model.Group;
import com.pearson.glp.lad.data.model.LearningExperience;
import com.pearson.glp.lad.data.model.MaxQuestion;
import com.pearson.glp.lad.data.model.Relationship;
import com.pearson.glp.lad.data.model.ResourcePlanItem;
import com.pearson.glp.lad.data.model.Scope;
import com.pearson.glp.lad.data.repository.AssetDeliveryRepository;
import com.pearson.glp.lad.model.constants.AdaptiveTypeEnum;
import com.pearson.glp.lad.model.constants.AssetClassEnum;
import com.pearson.glp.lad.model.constants.AssetTypeEnum;
import com.pearson.glp.lad.model.constants.ContentCategoryEnum;
import com.pearson.glp.lad.model.constants.DocumentTypeEnum;
import com.pearson.glp.lad.model.constants.ProductModelEnum;
import com.pearson.glp.lad.model.constants.ResourceTypeEnum;

import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 * The Class AssetDeliveryDataAccessTest.
 */
public class AssetDeliveryDataAccessTest {
	
	 
	
	/** The learning asset asset delivery data access. */
	@InjectMocks
	private AssetDeliveryDataAccess learningAssetAssetDeliveryDataAccess;
	
	/** The learning asset delivery repository. */
	@Mock
	private AssetDeliveryRepository learningAssetDeliveryRepository;
	
	/**
	   * Instantiates a new asset delivery data access test.
	   */
	  public AssetDeliveryDataAccessTest() {
	    super();
	  }
 
	  
	  /**
	   * Before method.
	   */
	  @Before
	  public void beforeMethod() {
	    MockitoAnnotations.initMocks(this);
	  }

	/**
	 * Test fetch learning experiences.
	 */
	@Test
	public void testFetchLearningExperiences() {
		
		String leId="68757a5d-c6c0-4d38-991b-dbea95e1a18b";
	 
		Mono<LearningExperience> leMono=getMonoLearningExperience();
		
		when(learningAssetDeliveryRepository.findById(leId)).thenReturn(leMono);
		
		StepVerifier.create(learningAssetAssetDeliveryDataAccess.fetchLearningExperiences(leId))
        .assertNext(res -> Assert.assertNotNull(leMono)).verifyComplete();
	}
	
	
	
	/**
	 * Test save learning experiences.
	 */
	@Test
	public void testSaveLearningExperiences() {
	   LearningExperience le=getLearningExperience();
	 
		Mono<LearningExperience> leMono=getMonoLearningExperience();
		
		when(learningAssetDeliveryRepository.save(le)).thenReturn(leMono);
		
		StepVerifier.create(learningAssetAssetDeliveryDataAccess.saveLearningExperiences(le))
        .assertNext(res -> Assert.assertNotNull(leMono)).verifyComplete();
	}
	
	
	 /**
	   * Gets the learning experience.
	   *
	   * @return the learning experience
	   */
	  private LearningExperience getLearningExperience() {

	    LearningExperience learningExperienceResponse = new LearningExperience();

	    String uid = "68757a5d-c6c0-4d38-991b-dbea95e1a18b";
	    learningExperienceResponse.setLeId(uid);

	    learningExperienceResponse.setBssVer(1);
	    learningExperienceResponse.setVer("a7bbcd8b-e1e2-48f2-a368-1b49f86e475c");

	    learningExperienceResponse.setCreated("2018-05-18T19:16:15+00:00");
	    learningExperienceResponse.setLastModified("2018-05-18T19:16:15+00:00");

	    learningExperienceResponse.setExpiresOn("2018-05-18T19:16:15+00:00");
	    learningExperienceResponse.setLabel(ProductModelEnum.REVEL);

	    String dummyString = "string";
	    learningExperienceResponse.setTags("tags");
	    learningExperienceResponse.setLanguage("en");
	    learningExperienceResponse.setDocType(DocumentTypeEnum.LEARNINGASSET);
	    learningExperienceResponse.setAssetType(AssetTypeEnum.LEARNINGEXPERIENCE);
	    learningExperienceResponse.setObjectives("objectveString");

	    Group groups = new Group();
	    learningExperienceResponse.setGroups(groups);

	    AssetResourceArray resources = new AssetResourceArray();
	    AssetInlinedResource assetInlinedResource1 = new AssetInlinedResource();

	    assetInlinedResource1.setResourceType(ResourceTypeEnum.INLINED);

	    assetInlinedResource1.setCategory(ContentCategoryEnum.NARRATIVE);

	    assetInlinedResource1.setModel("COURSEAVATAR");
	    assetInlinedResource1.setData("COURSETYPE");

	    resources.setCourseType(assetInlinedResource1);
	    learningExperienceResponse.setResources(resources);

	    AssetRelationship learningModel = new AssetRelationship();
	    learningModel.setAssetType(AssetTypeEnum.LEARNINGEXPERIENCE);
	    learningModel.setBssVer(learningExperienceResponse.getBssVer());
	    learningModel.setResourceType(ResourceTypeEnum.LEARNINGASSET);
	    learningModel.setLinks(new HashMap<>());

	    List<AssetGraphItem> assetGraph = new ArrayList<>();
	    AssetGraphItem assetGraphItem = new AssetGraphItem();
	    assetGraphItem.setStartNode(dummyString);
	    assetGraphItem.setEndNode(dummyString);
	    Relationship relationships = new Relationship(); //
	    assetGraphItem.setRelationships(relationships);
	    // assetGraph.add(assetGraphItem);

	    List<ResourcePlanItem> resourcePlan = new ArrayList<>();
	    ResourcePlanItem resourcePlanItem = new ResourcePlanItem();
	    resourcePlanItem.setLabel("REVEL");
	    resourcePlanItem.setResourceElementType(dummyString);
	    resourcePlanItem.setResourceRef(dummyString);
	    resourcePlan.add(resourcePlanItem);
	    learningExperienceResponse.setResourcePlan(resourcePlan);

	    Configuration configuration = new Configuration();
	    Engagement engagement = new Engagement();
	    Adaptive adaptive = new Adaptive();
	    adaptive.setCoreInstructions(Boolean.TRUE);
	    CourseMastery courseMastery = new CourseMastery();
	    CourseMasteryLevel courseMasteryLevel = new CourseMasteryLevel();
	    courseMasteryLevel.setLowerBound(50);
	    courseMasteryLevel.setMastery(90);
	    courseMasteryLevel.setUpperBound(70);
	    courseMastery.setPostAssessment(courseMasteryLevel);
	    courseMastery.setPractice(courseMasteryLevel);
	    courseMastery.setPreAssessment(courseMasteryLevel);
	    adaptive.setCourseMastery(courseMastery);
	    adaptive.setIsOverridable(Boolean.FALSE);
	    MaxQuestion maxQuestion = new MaxQuestion();
	    maxQuestion.setPostAssessment(1);
	    maxQuestion.setPracticeAssessment(maxQuestion.getPostAssessment());
	    maxQuestion.setPreAssessment(maxQuestion.getPostAssessment());
	    adaptive.setMaxQuestions(maxQuestion);
	    adaptive.setPostAssessment(Boolean.TRUE);
	    adaptive.setPracticeAssessment(Boolean.FALSE);
	    adaptive.setPreAssessment(Boolean.FALSE);
	    adaptive.setType(AdaptiveTypeEnum.DYNAMIC);
	    engagement.setAdaptive(adaptive);
	    // configuration.setExperienceEngagement(engagement);
	    learningExperienceResponse.setConfiguration(configuration);

	    List<String> constraints = Arrays.asList(dummyString, dummyString);
	    learningExperienceResponse.setConstraints(constraints);

	    Extends _extends = new Extends();
	    List<AssetRelationship> sources = new ArrayList<>();
	    AssetRelationship e = new AssetRelationship();
	    e.setAssetType(AssetTypeEnum.LEARNINGEXPERIENCE);
	    e.setDocType(DocumentTypeEnum.LEARNINGASSET);
	    e.setResourceType(ResourceTypeEnum.LEARNINGASSET);
	    sources.add(e);
	    _extends.setSources(sources);
	    learningExperienceResponse.setExtendz(_extends);

	    Map<String, String> extensions = new HashMap<>();
	    learningExperienceResponse.setExtensions(extensions);

	    learningExperienceResponse.setAssetClass(AssetClassEnum.COURSE);
	    learningExperienceResponse.setAssetGraph(assetGraph);
	    learningExperienceResponse.setDocType(DocumentTypeEnum.LEARNINGASSET);

	    Scope scope = new Scope();
	    learningExperienceResponse.setScope(scope);

	    return learningExperienceResponse;
	  }

	  /**
	   * Gets the mono learning experience.
	   *
	   * @return the mono learning experience
	   */
	  private Mono<LearningExperience> getMonoLearningExperience() {
	    return Mono.just(getLearningExperience());
	  }

}
